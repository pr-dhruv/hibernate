package com.soma;

import com.soma.dto.Address;
import com.soma.dto.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.io.*;
import java.util.Date;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Project Instantiation" );
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = sessionFactory.openSession();

        Student student = new Student();
        student.setName("Neo");
        student.setCity("John");
        student.setEmailAddress("demo@d.com");
        currentSession.beginTransaction();
        currentSession.save(student);
        currentSession.getTransaction().commit();

        byte[] image = null;
        try (FileInputStream fis = new FileInputStream("jerry.png")) {
            image = new byte[fis.available()];
            fis.read(image);
        } catch (IOException e) {
            e.printStackTrace();
        }


        Address address = new Address();
        address.setHouseNumber("524");
        address.setStreet("23rd Cross");;
        address.setRoad("20th Main Road");
        address.setLocality("Judicial Layout");
        address.setCity("Bangalore");
        address.setState("Karnataka");
        address.setPincode(560065);
        address.setDate(new Date());
        address.setImage(image);

        System.out.println(address);
        currentSession.beginTransaction();
        currentSession.save(address);
        currentSession.getTransaction().commit();

        currentSession.close();
    }
}
