package com.soma.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author Mahendra Prajapati
 * @since 30-08-2020
 */

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
@Entity
@Table(name = "student_details")
public class Student {

    @Id
    private String emailAddress;

    private String name;

    private String city;

    private Certificate certificate;

}
