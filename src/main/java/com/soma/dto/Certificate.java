package com.soma.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

/**
 * @author Mahendra Prajapati
 * @since 04-09-2020
 */

@Embeddable
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Certificate {
    private String id;
    private String course;

    @Temporal(TemporalType.DATE)
    private Date Expiry;
}
