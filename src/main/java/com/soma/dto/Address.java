package com.soma.dto;

import lombok.*;

import javax.persistence.*;
import java.util.Date;

/**
 * @author Mahendra Prajapati
 * @since 30-08-2020
 */

@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
@Entity
@Table(name = "student_address")    // To define the properties for the table
public class Address {

    @Id     // To define the primary key
    @GeneratedValue(strategy = GenerationType.IDENTITY) // To generate sequence values
    @Column(name = "address_id")    //  Defines the mapped column name for the attribute
    private int addressId;

    @Column(name = "house_number", length = 8)
    private String houseNumber;

    @Column(length = 100, name = "street")  // defines the name and length for the attribute
    private String street;

    @Column(length = 100, name = "road")
    private String road;

    @Column(length = 100, name = "locality")
    private String locality;

    @Column(length = 100, name = "city")
    private String city;

    @Column(length = 100, name = "state")
    private String state;

    @Column(length = 6, name = "pincode")
    private int pincode;

    @Column(name = "registration_date")
    @Temporal(TemporalType.TIMESTAMP)   // To change structure of the date and time
    private Date date;

    @Getter(value = AccessLevel.NONE)
    @Column(name = "address_proof")
    private byte[] image;

    @Transient  // This attribute is going to ignored by hibernate
    private int calculation;

    public boolean getImage() {
        return image != null;

    }

}
