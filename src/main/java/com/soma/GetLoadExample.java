package com.soma;

import com.soma.dto.Address;
import com.soma.dto.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author Mahendra Prajapati
 * @since 03-09-2020
 */
public class GetLoadExample {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = sessionFactory.openSession();

        // Get Example
        Student student = currentSession.get(Student.class, "demo@d.com");
        System.out.println(student);

        Address address = currentSession.get(Address.class, 6);
        System.out.println(address);

        student = null;
        address = null;
        System.out.println(student + " " + address);

        // Load Example
        student = currentSession.get(Student.class, "demo@d.com");
        System.out.println(student);

        address = currentSession.get(Address.class, 6);
        System.out.println(address);

        currentSession.close();
        sessionFactory.close();
    }
}
