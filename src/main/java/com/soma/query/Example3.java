package com.soma.query;

import com.soma.mapping.onetomany.dto.OTMAnswer;
import com.soma.mapping.onetomany.dto.OTMQuestion;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 11-09-2020
 */
public class Example3 {
    private static OTMQuestion question1, question2;
    private static OTMAnswer answer1, answer2, answer3, answer4, answer21, answer22, answer23, answer24;
    private static List<OTMAnswer> answerList, answerList1;
    private static final String ques = "What is Java?";
    private static final String ans11 = "High Level Programming language";
    private static final String ans12 = "Robust Language";
    private static final String ans13 = "Light weight Language";
    private static final String ans14 = "All of the above";

    private static final String ques2 = "What is Collections?";
    private static final String ans21 = "Utility class to perform operation in Collection objects like List and Set";
    private static final String ans22 = "Java Framework";
    private static final String ans23 = "Collection of Objects";
    private static final String ans24 = "All of the above";

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = null;
        currentSession = sessionFactory.openSession();
        if (currentSession == null)
            currentSession = sessionFactory.openSession();

        populateData();

        try {
            // Question 1 : What is Java?
            currentSession.beginTransaction();
            currentSession.save(answer1);
            currentSession.save(answer2);
            currentSession.save(answer3);
            currentSession.save(answer4);
            currentSession.save(question1);

            // Question 2 : What is Collections?
            currentSession.save(answer21);
            currentSession.save(answer22);
            currentSession.save(answer23);
            currentSession.save(answer24);
            currentSession.save(question2);
            currentSession.getTransaction().commit();

            // Join Query
            Query query = currentSession.createQuery("SELECT q.questionId, q.question, a.answer FROM OTMQuestion q INNER JOIN q.answers as a");
            List<Object[]> list = query.getResultList();
            System.out.println(list);
            for(Object[] arr : list) {
                System.out.println(Arrays.toString(arr));
            }
            /*OTMQuestion fetchedQuestion = currentSession.get(OTMQuestion.class, 1);
            System.out.println(fetchedQuestion.getQuestionId() + " -> " + fetchedQuestion.getQuestion());
            System.out.println(fetchedQuestion.getQuestion());
            for (OTMAnswer a: fetchedQuestion.getAnswers()) {
                System.out.println(a.getAnswer());
            }*/

        } catch (HibernateException e) {
            e.printStackTrace();
        }

        currentSession.close();
        sessionFactory.close();
    }

    private static void populateData() {
        // Question 1
        question1 = new OTMQuestion();
        question1.setQuestionId(1);
        question1.setQuestion(ques);

        answer1 = new OTMAnswer();
        answer1.setAnswerId(11);
        answer1.setAnswer(ans11);
        answer1.setQuestion(question1);

        answer2 = new OTMAnswer();
        answer2.setAnswerId(12);
        answer2.setAnswer(ans12);
        answer2.setQuestion(question1);

        answer3 = new OTMAnswer();
        answer3.setAnswerId(13);
        answer3.setAnswer(ans13);
        answer3.setQuestion(question1);

        answer4 = new OTMAnswer();
        answer4.setAnswerId(14);
        answer4.setAnswer(ans14);
        answer4.setQuestion(question1);

        answerList = new ArrayList<>();
        answerList.add(answer1);
        answerList.add(answer2);
        answerList.add(answer3);
        answerList.add(answer4);

        question1.setAnswers(answerList);

        // Question 2
        question2 = new OTMQuestion();
        question2.setQuestionId(2);
        question2.setQuestion(ques2);

        answer21 = new OTMAnswer();
        answer21.setAnswerId(21);
        answer21.setAnswer(ans21);
        answer21.setQuestion(question2);

        answer22 = new OTMAnswer();
        answer22.setAnswerId(22);
        answer22.setAnswer(ans22);
        answer22.setQuestion(question2);

        answer23 = new OTMAnswer();
        answer23.setAnswerId(23);
        answer23.setAnswer(ans23);
        answer23.setQuestion(question2);

        answer24 = new OTMAnswer();
        answer24.setAnswerId(24);
        answer24.setAnswer(ans24);
        answer24.setQuestion(question2);

        answerList1 = new ArrayList<>();
        answerList1.add(answer21);
        answerList1.add(answer22);
        answerList1.add(answer23);
        answerList1.add(answer24);

        question2.setAnswers(answerList1);

    }
}
