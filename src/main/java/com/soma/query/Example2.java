package com.soma.query;

import com.soma.dto.Certificate;
import com.soma.dto.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.Date;

/**
 * @author Mahendra Prajapati
 * @since 11-09-2020
 */
public class Example2 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = sessionFactory.openSession();

        Student student = new Student();
        student.setName("Mahendra");
        student.setCity("Jhansi");
        student.setEmailAddress("mahendra.prajapati@comviva.com");
        student.setCertificate(new Certificate("101", "Java", new Date()));

        Student student1 = new Student();
        student1.setName("Oshin");
        student1.setCity("Indore");
        student1.setEmailAddress("oshin.jain@comviva.com");
        student1.setCertificate(new Certificate("102", "React", new Date()));

        Student student2 = new Student();
        student2.setName("Sonal");
        student2.setCity("Morena");
        student2.setEmailAddress("sonal.goyal@amdocs.com");
        student2.setCertificate(new Certificate("103", "Junit", new Date()));

        Student student3 = new Student();
        student3.setName("Dharmendra");
        student3.setCity("Jhansi");
        student3.setEmailAddress("dharmendra.prajapati@epam.com");
        student3.setCertificate(new Certificate("104", "C# .Net", new Date()));


        currentSession.beginTransaction();
        currentSession.save(student);
        currentSession.save(student1);
        currentSession.save(student2);
        currentSession.save(student3);
        currentSession.getTransaction().commit();

        currentSession.beginTransaction();
        String queryString = "DELETE FROM Student as s where s.city=:city";
        Query query = currentSession.createQuery(queryString);
        query.setParameter("city", "Indore");
        System.out.println("No of query deleted : " + query.executeUpdate());
        currentSession.getTransaction().commit();

        currentSession.beginTransaction();
        queryString = "UPDATE Student SET city=:city where  emailAddress=:email";
        query = currentSession.createQuery(queryString);
        query.setParameter("city", "Jhansi");
        query.setParameter("email", "sonal.goyal@amdocs.com");
        System.out.println("No of query updated : " + query.executeUpdate());
        currentSession.getTransaction().commit();

        currentSession.close();
        sessionFactory.close();
    }
}
