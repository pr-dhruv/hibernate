package com.soma.query;

import com.soma.mapping.manytomany.dto.Emp;
import com.soma.mapping.manytomany.dto.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 10-09-2020
 */
public class Example1 {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = sessionFactory.openSession();

        // TODO code
        String queryString = "from Emp where empId=:empId";
        Query query = currentSession.createQuery(queryString);
        query.setParameter("empId", 1);
        List<Emp> empList = query.list();

        List<Project> projects = null;
        boolean isFirst = true;
        for (Emp emp : empList) {
            projects = emp.getProjectList();
            System.out.print(emp.getEmpId() + " -> " + emp.getEmpName());
            System.out.print(", Projects{");
            isFirst = true;
            for (Project project : projects) {
                if(isFirst) {
                    System.out.print(project.getProjectId() + "-> " + project.getProjectName());
                    isFirst = false;
                }
                else
                    System.out.print(", " + project.getProjectId() + "-> " + project.getProjectName());
            }
            System.out.println("}");
        }

        currentSession.close();
        sessionFactory.close();
    }
}
