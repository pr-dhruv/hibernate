package com.soma.mapping.onetoone.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * @author Mahendra Prajapati
 * @since 07-09-2020
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@Entity(name = "answer")
public class Answer {
    @Id
    @Column(name = "answer_id")
    private int answerId;

    private String answer;

    @OneToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "answer")
    @JoinColumn(name = "question_id")
    @JsonIgnore
    private Question question;

}
