package com.soma.mapping.onetoone;

import com.soma.mapping.onetoone.dto.Answer;
import com.soma.mapping.onetoone.dto.Question;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 * @author Mahendra Prajapati
 * @since 04-09-2020
 */

public class OneToOneMapping {

    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = null;
        currentSession = sessionFactory.openSession();
        if(currentSession == null)
            currentSession = sessionFactory.openSession();

        // TODO Code here

        Question question = new Question();
        question.setQuestionId(1);
        question.setQuestion("What is Java?");

        Answer answer = new Answer();
        answer.setAnswerId(11);
        answer.setAnswer("Java is high level programming language");
        answer.setQuestion(question);

        question.setAnswer(answer);

        currentSession.beginTransaction();
        currentSession.save(question);
        currentSession.save(answer);
        currentSession.getTransaction().commit();

        Question fetchedQuestion = currentSession.get(Question.class, 1);
        System.out.println(fetchedQuestion.getQuestionId() + ". " + fetchedQuestion.getQuestion());
        System.out.println(fetchedQuestion.getAnswer());


        currentSession.close();
        sessionFactory.close();
    }

}
