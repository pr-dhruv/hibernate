package com.soma.mapping.manytomany.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 09-09-2020
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Project {

    @Id
    @Column(name = "project_id")
    private int projectId;

    @Column(name = "project_name")
    private String projectName;

    @ManyToMany(mappedBy = "projectList", fetch = FetchType.LAZY)
    @JsonIgnore
    private List<Emp> empList;

}
