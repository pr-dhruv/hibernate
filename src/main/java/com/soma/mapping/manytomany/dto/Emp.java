package com.soma.mapping.manytomany.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 09-09-2020
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class Emp {

    @Id
    @Column(name = "emp_id")
    private int empId;

    @Column(name = "emp_name")
    private String empName;

    @ManyToMany
    @JoinTable(name = "emp_projects", joinColumns = {@JoinColumn(name = "emp_id")}, inverseJoinColumns = {@JoinColumn(name = "project_id")})
    private List<Project> projectList;
}
