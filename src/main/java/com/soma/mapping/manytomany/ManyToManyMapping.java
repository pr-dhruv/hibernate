package com.soma.mapping.manytomany;

import com.soma.mapping.manytomany.dto.Emp;
import com.soma.mapping.manytomany.dto.Project;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 09-09-2020
 */
public class ManyToManyMapping {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = sessionFactory.openSession();

        Project p1 = new Project();
        Project p2 = new Project();
        Project p3 = new Project();

        p1.setProjectId(11);
        p1.setProjectName("Mobiquity");

        p2.setProjectId(12);
        p2.setProjectName("Infinty");

        p3.setProjectId(13);
        p3.setProjectName("CVS");

        Emp emp1 = new Emp();
        Emp emp2 = new Emp();

        emp1.setEmpId(1);
        emp1.setEmpName("Mahendra");

        emp2.setEmpId(2);
        emp2.setEmpName("Oshin");

        List<Emp> empList = new ArrayList<>();
        List<Project> projectList = new ArrayList<>();

        empList.add(emp1);
        empList.add(emp2);

        projectList.add(p1);
        projectList.add(p2);

        emp1.setProjectList(projectList);
        p2.setEmpList(empList);

        currentSession.beginTransaction();
        currentSession.save(emp1);
        currentSession.save(emp2);
        currentSession.save(p1);
        currentSession.save(p2);
        currentSession.save(p3);
        currentSession.getTransaction().commit();

        currentSession.close();
        sessionFactory.close();
    }
}
