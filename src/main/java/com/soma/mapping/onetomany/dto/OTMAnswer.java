package com.soma.mapping.onetomany.dto;

/**
 * @author Mahendra Prajapati
 * @since 08-09-2020
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;

/**
 * @author Mahendra Prajapati
 * @since 07-09-2020
 */

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Data
@Entity
@Table(name = "otm_answer")
public class OTMAnswer {
    @Id
    @Column(name = "answer_id")
    private int answerId;

    private String answer;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinColumn(name = "question_id")
    @JsonIgnore
    private OTMQuestion question;

}
