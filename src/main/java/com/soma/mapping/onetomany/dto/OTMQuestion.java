package com.soma.mapping.onetomany.dto;

/**
 * @author Mahendra Prajapati
 * @since 08-09-2020
 */

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 06-09-2020
 */

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
@Table(name = "otm_question")
public class OTMQuestion {

    @Id
    @Column(name = "question_id")
    private int questionId;

    private String question;

    @OneToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL, mappedBy = "question")
    private List<OTMAnswer> answers;

}

