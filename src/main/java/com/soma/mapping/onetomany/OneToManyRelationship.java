package com.soma.mapping.onetomany;

import com.soma.mapping.onetomany.dto.OTMAnswer;
import com.soma.mapping.onetomany.dto.OTMQuestion;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Mahendra Prajapati
 * @since 08-09-2020
 */
public class OneToManyRelationship {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = null;
        currentSession = sessionFactory.openSession();
        if (currentSession == null)
            currentSession = sessionFactory.openSession();

        // TODO Code here

        String ques = "What is Java?";
        String ans1 = "High Level Programming language";
        String ans2 = "Robust Language";
        String ans3 = "Light weight Language";
        String ans4 = "All of the above";


        OTMQuestion question = new OTMQuestion();
        question.setQuestionId(1);
        question.setQuestion(ques);

        OTMAnswer answer1 = new OTMAnswer();
        answer1.setAnswerId(11);
        answer1.setAnswer(ans1);
        answer1.setQuestion(question);

        OTMAnswer answer2 = new OTMAnswer();
        answer2.setAnswerId(12);
        answer2.setAnswer(ans2);
        answer2.setQuestion(question);

        OTMAnswer answer3 = new OTMAnswer();
        answer3.setAnswerId(13);
        answer3.setAnswer(ans3);
        answer3.setQuestion(question);

        OTMAnswer answer4 = new OTMAnswer();
        answer4.setAnswerId(14);
        answer4.setAnswer(ans4);
        answer4.setQuestion(question);

        List<OTMAnswer> answerList = new ArrayList<>();
        answerList.add(answer1);
        answerList.add(answer2);
        answerList.add(answer3);
        answerList.add(answer4);

        question.setAnswers(answerList);

        try {

            currentSession.beginTransaction();
            currentSession.save(answer1);
            currentSession.save(answer2);
            currentSession.save(answer3);
            currentSession.save(answer4);

            currentSession.save(question);

            currentSession.getTransaction().commit();

            OTMQuestion fetchedQuestion = currentSession.get(OTMQuestion.class, 1);
            System.out.println(fetchedQuestion);
            System.out.println(fetchedQuestion.getQuestion());
            for (OTMAnswer a: fetchedQuestion.getAnswers()) {
                System.out.println(a.getAnswer());
            }

        } catch (HibernateException e) {
            e.printStackTrace();
        }

        currentSession.close();
        sessionFactory.close();
    }
}
