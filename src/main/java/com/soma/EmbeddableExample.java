package com.soma;

import com.soma.dto.Certificate;
import com.soma.dto.Student;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

import java.util.Date;


/**
 * @author Mahendra Prajapati
 * @since 04-09-2020
 */
public class EmbeddableExample {
    public static void main(String[] args) {
        Configuration configuration = new Configuration();
        configuration.configure("hibernate/hibernate.cfg.xml");
        SessionFactory sessionFactory = configuration.buildSessionFactory();

        Session currentSession = sessionFactory.openSession();

        Student s = new Student();
        Certificate certificate = new Certificate();
        certificate.setId("101");
        certificate.setCourse("Java");
        certificate.setExpiry(new Date());

        s.setName("John Doe");
        s.setEmailAddress("john@doe.com");
        s.setCity("New Jersy");
        s.setCertificate(certificate);

        currentSession.beginTransaction();
        currentSession.save(s);
        currentSession.getTransaction().commit();

        Student s1 = currentSession.get(Student.class, "john@doe.com");
        System.out.println(s1);
        currentSession.close();
        sessionFactory.close();
    }
}
